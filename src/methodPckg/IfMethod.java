package methodPckg;

import types.Atom;
import types.BooleanType;
import types.SExpression;
import main.Command;
import main.EvaluateMethods;

public class IfMethod extends EvaluateMethods implements Command{

	/**
	 * If method
	 * evaluates the first expression,
	 * if true the next expression is executed (3rd parameter)
	 * if false the following expression is evaluated (4th parameter)
	 * e.g. ( if #f expr1 expr2 ) would evaluate expr2
	 */
	@Override
	public SExpression execute(SExpression expr) {
		Atom value;
		SExpression boolExpr;
		boolExpr = evaluate((SExpression) expr);
		if(!(boolExpr.getData() instanceof Atom)){
			System.out.println("First parameter of condition must evaluate to an Atom");
			error();
		}
		value = (Atom) boolExpr.getData();
		if(value.equals(BooleanType.valueOf(true))){
			expr = expr.getNext();
		}else if(value.equals(BooleanType.valueOf(false))){
			expr = expr.getNext().getNext();
			if(expr.getData() == null){
				return null;
			}
		}else{
			System.out.println("first parameter must evaluate to a boolean");
			error();
		}
		return evaluate(expr);
	}

}
