package methodPckg;

import types.Environment;
import types.SExpression;
import main.Command;
import main.Driver;
import main.EvaluateMethods;

public class FunMethod extends EvaluateMethods implements Command {

	// ( fun 
	@Override
	public SExpression execute(SExpression expr) {
		Driver.currentEnvironment = new Environment(Driver.currentEnvironment);
		addNameValueParametersToEnvironment(expr);
		expr = expr.getNext();
		SExpression result = evaluate(expr);	
		Driver.currentEnvironment = Driver.currentEnvironment.getParent();
		return null;
	}

}
