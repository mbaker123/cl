package methodPckg;

import main.Command;
import main.EvaluateMethods;
import types.SExpression;

public class CarMethod extends EvaluateMethods implements Command {
	/**
	 * Car method:
	 * Evaluates the expression
	 * Copies the list from the current level downwards
	 * Returns the first element of the list with the next and parents removed
	 * @param expr the list the first element should be retrieved from
	 * @return
	 */
	@Override
	public SExpression execute(SExpression expr) {
		SExpression head;
		
		expr = evaluate(expr);

		if(!(expr.getData() instanceof SExpression)){
			System.out.println("Invalid parameter - car takes a non empty list");
			error();
		}
		
		head = copyExpression((SExpression) expr.getData());
		
		head.setNext(SExpression.newDefaultSExpression());
		return head;
	
	}

}