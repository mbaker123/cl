package methodPckg;
import main.Command;
import main.Driver;
import main.EvaluateMethods;
import types.Atom;
import types.Definition;
import types.SExpression;

public class DefineMethod extends EvaluateMethods implements Command{

	/**
	 * Evaluates a define function
	 * Creates a definition with a name and an SExpression of code to the current environment
	 * @param expr data is the name, next is a pointer to the code
	 */
	
	@Override
	public SExpression execute(SExpression expr) {
		addToNameValueParameterToEnvironment(expr);
		SExpression value = newDefaultSExpressionWithNext();
		value.setData(expr.getData());
		return value;
	}


}
