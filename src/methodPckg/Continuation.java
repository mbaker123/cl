package methodPckg;

import types.SExpression;
import main.Command;
import main.EvaluateMethods;

public class Continuation { 

	public SExpression value;
	ContinuationException c; 

	public Continuation(ContinuationException cc) {
		c = cc;
	}


	public SExpression evalContinuation(SExpression expr) throws ContinuationException {
		//Get the first element of the continuation
		value = expr.getNext();
		throw c;
	}
}
