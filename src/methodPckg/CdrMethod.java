package methodPckg;
import main.Command;
import main.EvaluateMethods;
import types.Atom;
import types.SExpression;


public class CdrMethod extends EvaluateMethods implements Command{
	
	/**
	 * Evaluates the expression
	 * copies the existing list
	 * removes the first element, changes parent pointer to the second element
	 * @param expr a list of at least one element
	 * @return a pointer to the parent of the cdr'd list
	 */
	
	@Override
	public SExpression execute(SExpression expr){
		SExpression tail;

		expr = (SExpression) evaluate(expr);
		
		if(expr.getData() instanceof Atom){
			System.out.println("parameter error - cdr takes a non empty list");
			error();
		}

		tail = ((SExpression) expr.getData()).getNext();
		return newParent(tail);	
	}
}
