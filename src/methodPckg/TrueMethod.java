package methodPckg;

import main.Command;
import main.EvaluateMethods;
import types.BooleanType;
import types.SExpression;

public class TrueMethod extends EvaluateMethods implements Command {
	/**
	 * Returns the boolean true
	 */
	@Override
	public SExpression execute(SExpression expr) {
		return newSExpressionFromAtom(BooleanType.valueOf(true));
	}
}