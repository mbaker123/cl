package methodPckg;
import main.Command;
import main.EvaluateMethods;
import types.SExpression;


public class ConsMethod extends EvaluateMethods implements Command {

	/**
	 * evaluates both parameters then
	 * creates a copy of list and lower levels (not the parents) 
	 * adds toAdd to the head and changes all top level parents to a new parent
	 * @param toAdd data to be added to the head of the list
	 * @param list the list to be modified
	 * @return pointer to the copied list parent
	 */

	@Override
	public SExpression execute(SExpression expr) {
		SExpression data = expr;
		SExpression list = expr.getNext();
		
		data = evaluate(data);
		list = evaluate(list);

		if(!(list.getData() instanceof SExpression)){
			System.out.println("Error - second argument of cons must be a list");
			error();
		}

		data.setParent(list);
		data.setNext((SExpression) list.getData());
		list.setData(data);	
		return replaceParent(list);
	}
}
