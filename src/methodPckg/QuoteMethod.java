package methodPckg;

import types.SExpression;
import main.Command;
import main.EvaluateMethods;

public class QuoteMethod extends EvaluateMethods implements Command {

	/**
	 * Suppresses the expression so it doesn't get evaluated
	 */
	@Override
	public SExpression execute(SExpression expr) {
		expr.setParent(null);
		expr.setNext(SExpression.newDefaultSExpression());
		return expr;
	}	
}
