package methodPckg;

import main.Command;
import main.Driver;
import main.EvaluateMethods;
import types.Environment;
import types.SExpression;

public class LetMethod extends EvaluateMethods implements Command {

	/**
	 * Example : ( let ( ( x 1 ) ( y 2 ) ) ( + x y ) )
	 */
	@Override
	public SExpression execute(SExpression expr) {
		Driver.currentEnvironment = new Environment(Driver.currentEnvironment);
		addNameValueParametersToEnvironment(expr);
		expr = expr.getNext();
		SExpression result = evaluate(expr);	
		Driver.currentEnvironment = Driver.currentEnvironment.getParent();
		return result;
	}	
}