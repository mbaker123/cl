package methodPckg;

import java.util.ArrayList;
import java.util.List;

import sun.awt.SunHints.Value;
import types.AtomFactory;
import types.Expression;
import types.SExpression;
import main.Command;
import main.EvaluateMethods;

public class MapMethod extends EvaluateMethods implements Command{

	/**
	 * ( map add ( list 1 2 3 ) ( list 4 5 6 ) )
	 * ( map add ( list 1 ) ( list 4 ) )
	 * -> (5 7 9)
	 */
	@Override
	public SExpression execute(SExpression expr) {
		Expression operateur;// because can be an expression or an operateur
		SExpression result = newSExpressionFromAtom(AtomFactory.valueOf("list"));
		List<SExpression> listOfList = new ArrayList<SExpression>(); 

		operateur = expr.getData();
		SExpression currList = expr.getNext();
		while(currList.getData() != null){
			if(!(currList.getData() instanceof SExpression)){
				System.out.println("That have to be an List");
				error();
			}
			listOfList.add(evaluate((SExpression)currList.getData()));
			currList = currList.getNext();
		}

		if(listOfList.size() == 0)
		{
			System.out.println("The argument for the map have to be map F List List");
			error();
		}


		List<SExpression> row = new ArrayList<SExpression>();
		currList = result;
		boolean first = true;
		while(listOfList.get(0).getData() != null){
			for(int i = 0;  i < listOfList.size(); i++){
				SExpression mycurrentExp = listOfList.get(i);
				if(first && (mycurrentExp.getData() instanceof SExpression)){
					mycurrentExp = (SExpression)mycurrentExp.getData();
				}
				row.add(mycurrentExp);
				listOfList.set(i,  mycurrentExp.getNext());
			}
			if(first) first = false;
			currList.setNext(evalRow(operateur, row));
			currList = currList.getNext();
			row.clear();
		}
		
		return evaluate(result);
	}

	public SExpression evalRow (Expression F,  List<SExpression> list){
		SExpression expr = newDefaultSExpressionWithNext();
		expr.setData(F);
		SExpression cur = expr.getNext();
		for(SExpression exp : list){
			cur.setNext(newDefaultSExpressionWithNext());
			cur.setData(exp.getData());
			cur = cur.getNext();
		}
		return evaluate(expr);
	}

}
