package methodPckg;

import main.Command;
import main.EvaluateMethods;
import types.Atom;
import types.SExpression;

public class MultMethod extends EvaluateMethods implements Command{
	
	/**
	 * mult the evaluated expression to the next until the null data is reached
	 * @param expr List of elements that evaluate to Atoms 
	 * @return Minus of the Atoms
	 */
	@Override
	public SExpression execute(SExpression expr) {
		int total = 1;

		while(expr.getData() != null){
			try{
				SExpression e = evaluate(expr);
				if(e.getData() instanceof Atom)
					total *= ((Atom) e.getData()).toInt();
				else{
					System.out.println("Invalid parameters");
					error();
				}
			}catch(NumberFormatException ex){
				System.out.println("Invalid parameters");
				error();
			}
			expr = expr.getNext();
		}
		return newSExpressionFromAtom(new Atom(Integer.toString(total)));
	}
	
}
