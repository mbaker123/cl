package methodPckg;

import types.SExpression;
import main.Command;
import main.EvaluateMethods;

public class CondMethod extends EvaluateMethods implements Command { 

	/**
	 * Cond Method
	 * takes a number of lists that are evaluated and 
	 */
	@Override
	public SExpression execute(SExpression expr) {
		SExpression result = null;
		while(result == null && expr.getData() != null){
			if(!(expr.getData() instanceof SExpression)){
				System.out.println("Each parameter of Cond must be a list");
				error();
			}
			Command tmp = new IfMethod();
			result = tmp.execute((SExpression) expr.getData());
			expr = expr.getNext();
		}
		return result;
	}
	
}
