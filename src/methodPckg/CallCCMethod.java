package methodPckg;

import types.Environment;
import types.SExpression;
import main.Command;
import main.Driver;
import main.EvaluateMethods;

public class CallCCMethod extends EvaluateMethods implements Command{

	@Override
	public SExpression execute(SExpression expr) {
		SExpression result = expr;
		//Create a new Environnement
		Driver.currentEnvironment = new Environment(Driver.currentEnvironment);
		addNameValueParametersToEnvironment(expr);
		ContinuationException cc = new ContinuationException();
	    Continuation proc = new Continuation(cc);
	    try { 
	    	result = proc.evalContinuation(expr); 
	    }catch (ContinuationException e) { 
	      if (e == cc) 
	    	  result =  proc.value;
	      else 
	    	  e.printStackTrace(); 
	    }
	    //Evaluate the first element;
	    result = evaluate(result);
		return result;//return the first element
	}

}
