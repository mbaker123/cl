package methodPckg;

import main.Command;
import main.EvaluateMethods;
import types.SExpression;

public class EvalMethod extends EvaluateMethods implements Command {
	@Override
	public SExpression execute(SExpression expr) {
		expr.setParent(null);
		expr.setNext(SExpression.newDefaultSExpression());
		return evaluate(evaluate(expr));
	}	
}
