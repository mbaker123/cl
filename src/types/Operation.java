package types;

import java.util.HashMap;

public class Operation extends Atom{
	//Operation
	public static final String opAddStr = "+";
	public static final String opAddStrStr = "add";
	public static final String opMultStr = "*";
	public static final String opMinusStr = "-";
	public static final String opDivStr = "/";	
	//Other
	public static final String opCarStr = "car";
	public static final String opCdrStr = "cdr";
	public static final String opConsStr = "cons";
	public static final String opQuoteStr = "quote";
	public static final String opQuoteShrtStr = "'";
	public static final String opDefineStr = "define";
	public static final String opIfStr = "if";
	public static final String opEvalStr = "eval";
	public static final String opElseStr = "else";
	public static final String opLetStr = "let";
	public static final String opCondStr = "cond";
	//other thing i add
	public static final String opListStr = "list";
	public static final String opMapStr = "map";
	public static final String opLambdaStr = "lambda";
	public static final String opcallcc = "call-with-current-continuation";
	
	//Operation
	private static final Operation opAdd = new Operation(opAddStr);
	private static final Operation opMinus = new Operation(opMinusStr);
	private static final Operation opDiv = new Operation(opMinusStr);
	private static final Operation opMult = new Operation(opMultStr);
	//Other
	private static final Operation opCar = new Operation(opCarStr);
	private static final Operation opCdr = new Operation(opCdrStr);
	private static final Operation opCons = new Operation(opConsStr);
	private static final Operation opQuote = new Operation(opQuoteShrtStr);
	private static final Operation opDefine = new Operation(opDefineStr);
	private static final Operation opIf = new Operation(opIfStr);
	private static final Operation opEval = new Operation(opEvalStr);
	private static final Operation opElse = new Operation(opElseStr);	
	private static final Operation opLet = new Operation(opLetStr);
	private static final Operation opCond = new Operation(opCondStr);
	//other thing i add
	private static final Operation opList = new Operation(opListStr);
	private static final Operation opMap = new Operation(opMapStr);
	private static final Operation opLambda = new Operation(opLambdaStr);
	private static final Operation opCallcc = new Operation(opcallcc);
		
	public static final HashMap<String, Operation> mapOfVals;
	static{
		mapOfVals = new HashMap<String, Operation>();
		mapOfVals.put(opAddStr, opAdd);
		mapOfVals.put(opAddStrStr, opAdd);
		mapOfVals.put(opMinusStr, opMinus);
		mapOfVals.put(opMultStr, opMult);
		mapOfVals.put(opDivStr, opDiv);
		
		mapOfVals.put(opCarStr, opCar);
		mapOfVals.put(opCdrStr, opCdr);
		mapOfVals.put(opConsStr, opCons);
		mapOfVals.put(opQuoteStr, opQuote);
		mapOfVals.put(opQuoteShrtStr, opQuote);
		mapOfVals.put(opDefineStr, opDefine);
		mapOfVals.put(opIfStr, opIf);
		mapOfVals.put(opEvalStr, opEval);
		mapOfVals.put(opElseStr, opElse);
		mapOfVals.put(opLetStr, opLet);
		mapOfVals.put(opCondStr, opCond);
		
		//What I add
		mapOfVals.put(opListStr, opList);
		mapOfVals.put(opMapStr, opMap);
		mapOfVals.put(opLambdaStr, opLambda);
		mapOfVals.put(opcallcc, opCallcc);
	}
	
	private Operation(String value) {
		super(value);
	}
	
	public static Operation valueOf(String value){
		return mapOfVals.get(value);
	}
	
	public static boolean isOperation(String value){
		return mapOfVals.containsKey(value);
	}
}
