package types;

public interface Expression {
	public Object clone() throws CloneNotSupportedException;
}
