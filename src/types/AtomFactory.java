package types;

public class AtomFactory {
	private AtomFactory(){
		
	}
	
	public static Atom valueOf(String value){
		if(Operation.isOperation(value)){
			return Operation.valueOf(value);
		}else if(BooleanType.isBooleanType(value)){
			return BooleanType.valueOf(value);
		}else{
			return new Atom(value);
		}
	}
	
}
