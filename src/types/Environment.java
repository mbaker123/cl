package types;
import java.util.ArrayList;


public class Environment {
	private ArrayList<Definition> variables = new ArrayList<>();
	private Environment parent;
	
	public Environment(){}
	
	public Environment(Environment parent){
		this.parent = parent;
	}
	
	public void addDefinition(Definition d){
		variables.add(d);
	}
	
	public Environment getParent(){
		return parent;
	}
	
	public ArrayList<Definition> getDefinitions(){
		return variables;
	}
	
	public Definition getDefinition(Atom name){
		for(Definition tmp: variables){
			if(tmp.getName().equals(name.getValue()));
				return tmp;
		}
		return null;
	}
	
}
