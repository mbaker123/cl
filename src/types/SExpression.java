package types;

public class SExpression implements Expression, Cloneable{
	private SExpression parent;
	private SExpression next;
	private Expression data;

	private static SExpression curParent;

	private SExpression (){	
	}

	public static SExpression newDefaultSExpression(){
		return new SExpression();
	}

	public static SExpression newSExpressionWithParent(SExpression parent){
		SExpression tmp = newDefaultSExpression();
		tmp.setParent(parent);
		return tmp;
	}

	public static SExpression newSExpressionWithData(Expression data){
		SExpression tmp = newDefaultSExpression();
		tmp.setData(data);
		return tmp;		
	}

	public void setParent(SExpression parent){
		this.parent = parent;
	}

	public void setData(Expression data){
		this.data = data;
	}

	public void setNext(SExpression next){
		this.next = next;
	}

	public SExpression getParent() {
		return parent;
	}

	public SExpression getNext() {
		return next;
	}

	public Expression getData() {
		return data;
	}


	@Override
	public Object clone() throws CloneNotSupportedException {
		SExpression clonedExpr = (SExpression) super.clone();
		clonedExpr.setParent(curParent);
		if(clonedExpr.getData() != null){
			curParent = clonedExpr;
			clonedExpr.setData((Expression) clonedExpr.data.clone());
		}if(clonedExpr.getNext() != null){
			curParent = clonedExpr.getParent();
			clonedExpr.setNext((SExpression) clonedExpr.next.clone());
		}
		return clonedExpr;
	}

	public SExpression deepClone(){
		try {
			return (SExpression) clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}
}
