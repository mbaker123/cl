package types;

public class Atom implements Expression, Cloneable{

	private final String value;

	public Atom(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}
	
	@Override
	public String toString(){
		return value;
	}
	
	public int toInt(){
		return Integer.parseInt(getValue());
	}

	@Override
	public boolean equals(Object obj){
		if(obj == null)
			return false;

		if(!(obj instanceof Atom))
			return false;

		Atom other = (Atom) obj;
		
		if(!(this.value.equals(other.value)))
			return false;
		
		return true;
	}
	
	@Override
	public int hashCode(){
		return value.hashCode();
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
