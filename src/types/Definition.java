package types;

public class Definition{
	private Atom name;
	private SExpression code;
	
	public Definition(Atom name, SExpression code){
		this.name = name;
		this.code = code;
	}

	public Atom getName() {
		return name;
	}

	public SExpression getCode() {
		return code;
	}
	
	public String toString(){
		return name.getValue();
	}
	
}
