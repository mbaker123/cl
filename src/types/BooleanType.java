package types;

public class BooleanType extends Atom {
	private static final BooleanType atmTrue = new BooleanType("#t");
	private static final BooleanType atmFalse = new BooleanType("#f");
		
	private BooleanType(String value){
		super(value);
	}
	
	public static BooleanType valueOf(boolean value){
		return value ? atmTrue : atmFalse;
	}
	
	public static BooleanType valueOf(String value){
		if(isBooleanType(value))
			return value.equalsIgnoreCase("#t") ? atmTrue : atmFalse;
		return null;
	}
	
	public static boolean isBooleanType(String value){
		if(value.equalsIgnoreCase("#t") || value.equalsIgnoreCase("#f"))
			return true;
		return false;
	}
}
