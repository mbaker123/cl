package main;

import types.*;

public class EvaluateMethods {
	
	/**
	 * Evaluate all express 
	 * that can be an atom like 2 or f that was defined precedetly
	 * @param expr
	 * @return
	 */
	public static Expression evaluate(Expression expr){
		if(expr instanceof SExpression)
			return evaluate((SExpression) expr);
		if(expr instanceof Atom)
			return handleAtom((Atom) expr);
		return expr;
	}
	
	/**
	 * Evaluates the Expression, handles lists, operations and defined functions
	 * Needs to error the evaluation of an SExpression or something? so ((+ 5 6)) is error should be (+ 5 6)
	 * @param data to be evaluated
	 * @return the result of the evaluation
	 */
	public static SExpression evaluate(SExpression expr){
		
		if(expr.getData() instanceof SExpression){
			return evaluate((SExpression) expr.getData());
		}
		if(expr.getData() instanceof Operation){
			Operation operation = (Operation) expr.getData();
			Command cmd = Driver.nameToOperation.get((operation.getValue()));
			return cmd.execute(expr.getNext());
		}else if(expr.getData() instanceof Atom){
			return handleAtom((Atom)expr.getData());
		}
		return null;
	}
	
	/**
	 * Handles the evaluation of an Atom, checks if a defined procedure if so create new environment and evaluate 
	 * else return copy of atom
	 * @param value to be evaluated
	 * @return returns evaluated code or a copy of itself
	 */
	private static SExpression handleAtom(Atom value){
		Environment currentEnvironment = Driver.currentEnvironment;
		if(isEnvironmentVar(value, currentEnvironment)){
			return runProcedure(value);
		}
		return newSExpressionFromAtom(value);
	}	
	
	/**
	 * Gets the code from the Environment that matches the parameter name
	 * Creates a new environment
	 * Evaluate the code
	 * Reverts back to the original environment
	 * Returns the result
	 * @param value the parameter name
	 * @return the Result of the procedure
	 */
	private static SExpression runProcedure(Atom value){
		SExpression code = resolveVar(value);
		Driver.currentEnvironment = new Environment(Driver.currentEnvironment);			
		SExpression result = evaluate(code);
		Driver.currentEnvironment = Driver.currentEnvironment.getParent();			
		return result;
	}
	
	/**
	 * Adds a list of parameters to the current environment
	 * @param expr parameters in the form ( ( x 1 )( y 2 ) )
	 */
	public static void addNameValueParametersToEnvironment(SExpression expr){
		if(!(expr.getData() instanceof SExpression)){
			System.out.println("List of parameters must be a list");
			error();
		}
		expr = (SExpression) expr.getData();
		while(expr.getData() != null){
			if(!(expr.getData() instanceof SExpression)){
				System.out.println("Each parameter must be a name value list");
				error();
			}
			addToNameValueParameterToEnvironment((SExpression) expr.getData());
			expr =  expr.getNext();
		}
	}
	
	/**
	 * Adds a definition to the environment 
	 * @param expr 1st parameter is name, 2nd is code
	 */
	public static void addToNameValueParameterToEnvironment(SExpression expr){
		Driver.currentEnvironment.addDefinition(newDefinition(expr));
	}
	
	/**
	 * returns a new definition from the expression
	 * @param expr data should be the procedure name, next should be the code
	 * @return instance of a definition from the expression
	 */
	
	public static Definition newDefinition(SExpression expr){
		Atom procedureName;
		SExpression code;

		if(!(expr.getData() instanceof Atom)){
			System.out.println("First paramater must be an Atom");
			error();
		}

		procedureName = (Atom) expr.getData();
		code = expr.getNext();
		
		if(code.getData() == null){
			System.out.println("Second parameter cannot be null");
			error();
		}
		
		code = expr.getNext();
		return new Definition(procedureName, code);
	}
	
	/**
	 * Checks if the procedure exists in the current environment or a parent environment
	 * @param procedureName the procedure name to be checked
	 * @return true if the procedure is found
	 */
	public static boolean isEnvironmentVar(Atom procedureName, Environment environement){
		if(environement == null)
			return false;
		
		for(Definition tmp: environement.getDefinitions()){
			if(tmp.getName().getValue().equals(procedureName.getValue()))
				return true;
		}
		return isEnvironmentVar(procedureName, environement.getParent());
	}
	
	/**
	 * Replaces the parent with a parentless SExpression, then copies all objects from the parent downwards
	 * @param expr list to be modified
	 * @return the parent (copied version)
	 */
	public static SExpression copyAndReplaceParent(SExpression expr){
		expr = replaceParent(expr);
		return (SExpression) copyExpression(expr);
	}
	
	/**
	 * With an element from a list, a new parent is created and the data/parent references are set
	 * @param expr an element of a list
	 * @return the new parent
	 */
	public static SExpression newParent(SExpression expr){
		SExpression parent = newDefaultSExpressionWithNext();
		parent.setData(expr);
		setChildrenParent(parent);
		return parent;
	}
	
	/**
	 * creates an SExpression with an empty SExpression on the next, null data/parent
	 * @return a new default SExpression
	 */
	public static SExpression newDefaultSExpressionWithNext(){
		SExpression expr = SExpression.newDefaultSExpression();
		expr.setNext(SExpression.newDefaultSExpression());
		return expr;
	}

	/**
	 * Replaces the parent SExpression with a parentless SExpression, handles references
	 * @param expr a list
	 * @return the parent
	 */
	public static SExpression replaceParent(SExpression expr){
		SExpression newParent = newDefaultSExpressionWithNext();
		newParent.setData(expr.getData());
		return setChildrenParent(newParent);
	}
	
	public static SExpression setChildrenParent(SExpression expr){
		SExpression child = (SExpression) expr.getData();
		
		while(child.getData() != null){
			child.setParent(expr);
			child = child.getNext();
		}
		return expr;
	}

	/**
	 * Copies all objects from the expression downwards
	 * @param expr to be copied
	 * @return copy of the expression
	 */
	public static SExpression copyExpression(SExpression expr){
		return expr.deepClone();
	}

	/**
	 * turns a function name into the code it matches up with in the environment
	 * @param var procedure name
	 * @return the code to be executed, null if not found
	 */
	private static SExpression resolveVar(Atom var){
		Environment tmpEnvironment;
		tmpEnvironment = Driver.currentEnvironment;
		while(tmpEnvironment != null){
			for(Definition tmp: tmpEnvironment.getDefinitions()){
				if(tmp.getName().getValue().equals(var.getValue()))
					return copyExpression(tmp.getCode());
			}
			tmpEnvironment = tmpEnvironment.getParent();
		}
		return null;
	}

	/**
	 * Turns an Atom into an SExpression of that atom with a pointer to an SExpression with null values
	 * This can be used to turn an atom into the dotted pair representation
	 * @param data Atom to be turned into SExpression
	 * @return SExpression with data as the Atom, next as a new default SExpression
	 */
	public static SExpression newSExpressionFromAtom(Atom data){
		SExpression expr = SExpression.newSExpressionWithData(data);
		expr.setNext(SExpression.newDefaultSExpression());
		return expr;
	}
	
	/**
	 * Error, currently unused
	 */
	public static void error(){
		System.out.println("Error!");
		System.exit(0);
	}
}
