package main;

import java.util.HashMap;

import types.*;
import methodPckg.*;

public class Driver {
	public static final HashMap<String, Command> nameToOperation= new HashMap<String, Command>();
	public static Environment currentEnvironment = new Environment();

	/**
	 * Parses the scheme expression input then sends this data into a read eval print loop
	 * @param args space separated scheme input
	 */
	public Driver(){
		initOpToParameterMap();

	}

	public void interpret(String[] args)
	{
		SExpression data = buildExpression(args);
		readEvalPrintLoop(data);
	}

	/**
	 * Initiates the HashMap that converts the symbol to an Operation
	 * This ensures there are only ever N instances of Operation created at any point
	 */
	private void initOpToParameterMap(){		
		try {
			nameToOperation.put(Operation.opAddStr, new AddMethod());
			nameToOperation.put(Operation.opAddStrStr, new AddMethod());
			nameToOperation.put(Operation.opMinusStr, new MinusMethod());
			nameToOperation.put(Operation.opDivStr, new DivMethod());
			nameToOperation.put(Operation.opMultStr, new MultMethod());

			nameToOperation.put(Operation.opCarStr, new CarMethod());
			nameToOperation.put(Operation.opCdrStr, new CdrMethod());
			nameToOperation.put(Operation.opConsStr, new ConsMethod());
			nameToOperation.put(Operation.opConsStr, new QuoteMethod());
			nameToOperation.put(Operation.opQuoteStr, new QuoteMethod());	
			nameToOperation.put(Operation.opQuoteShrtStr, new QuoteMethod());	
			nameToOperation.put(Operation.opDefineStr, new DefineMethod());
			nameToOperation.put(Operation.opIfStr, new IfMethod());
			nameToOperation.put(Operation.opEvalStr, new EvalMethod());
			nameToOperation.put(Operation.opElseStr, new TrueMethod());
			nameToOperation.put(Operation.opLetStr, new LetMethod());
			nameToOperation.put(Operation.opCondStr, new CondMethod());
			
			//other 
			nameToOperation.put(Operation.opListStr, new ListMethod());
			nameToOperation.put(Operation.opMapStr, new MapMethod());
			nameToOperation.put(Operation.opLambdaStr, new LambdaMethod());
			nameToOperation.put(Operation.opcallcc, new CallCCMethod());
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	/**
	 * evaluates each dotted pair and prints the result then loops around
	 * @param currentExpr dotted pair representation of scheme code
	 */
	private void readEvalPrintLoop(SExpression currentExpr){
		while(currentExpr.getData() != null){
			Expression exp =  EvaluateMethods.evaluate((Expression) currentExpr.getData());
			if(exp instanceof SExpression)
				print((SExpression) exp);
			else
				print((Atom) exp);
			currentExpr = currentExpr.getNext();
		}
	}

	/**
	 * Converts text input to dotted pairs (SExpressions) functions in the opList are Operations and 
	 * everything else is an Atom. New SExpressions are created based on brackets
	 * @param code space separated input scheme code
	 * @return Dotted pairs representation of input
	 */
	private SExpression buildExpression(String[] code){
		SExpression expr = SExpression.newDefaultSExpression();
		SExpression start = expr;

		for(String element: code){
			element = element.trim().toLowerCase();
			if(!element.equals(""))
			{
				switch(element){
				case "(":
					expr.setData(SExpression.newSExpressionWithParent(expr));
					expr = (SExpression) expr.getData();
					break;
				case ")":
					expr = expr.getParent();
					expr.setNext(SExpression.newSExpressionWithParent(expr.getParent()));
					expr = expr.getNext();
					break;
				default:

					expr.setData(AtomFactory.valueOf(element));
					expr.setNext(SExpression.newSExpressionWithParent(expr.getParent()));
					expr = expr.getNext();
					break;
				}
			}
		}
		return start;
	}

	/**
	 * Prints either an Atom or SExpression in a human readable form
	 * @param sExpr Expression to be printed
	 */
	public static void print(SExpression expr){
		if(expr == null){
			System.out.println("Null Expression");
			return;
		}

		while(expr.getParent() != null || expr.getData() != null){
			if(expr.getData() == null){
				System.out.print(") ");
				expr = expr.getParent().getNext();
			}else if(expr.getData() instanceof SExpression){
				System.out.print("( ");
				expr = (SExpression) expr.getData();
			}else{
				System.out.print(((Atom) expr.getData()).getValue()+ " ");
				expr = expr.getNext();
			}
		}
		System.out.println();
	}
	
	public static void print(Atom expr){
		if(expr == null){
			System.out.println("Null Expression");
			return;
		}
		System.out.println(expr.getValue());
	}

}
