package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		try{
			System.out.println("Welcome to this project.");
			System.out.println("It's a easy interpreteur jScheme");
			
			Driver driver = new Driver();
			while(true){
				System.out.print("> ");
				BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
				String s = bufferRead.readLine();
				driver.interpret(s.split(" "));
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

	}

}
