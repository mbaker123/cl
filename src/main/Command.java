package main;

import types.SExpression;

public interface Command {
	public SExpression execute(SExpression expr);
}
