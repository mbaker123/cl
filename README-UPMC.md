Member of the team :
 	- 	BAKER MAISSA : mayssa.baker@gmail.com
 	- 	LOUIZA BOULILA : louizaboulila.etu@gmail.com

Hello this project was taken from this source : https://github.com/BigKie1/SchemeInterpreter
it's a project made by a student.
That's an interpreteur JScheme with only basic function.

When we started working on this project 
we find a lot of non functionning operation scheme

So We added this functionnality :

	-	Mult  : Example  ( * 1 3 )
	- 	DIV   : Example  ( / 5 5 )
	-	Minus : Example  ( - 5 1 )

	- 	DEFINE that doesn't worded
		EXAMPLE :   > ( define f 4 )
				    > f

	- LET  that also doest worked
		EXAMPLE  : > ( let ( ( x 1 ) ( y 4 ) ) ( + x y ) )
	- LIST :  Example ( list 1 2 3 4 )
	- MAP  :  EXample ( map ( list 1 2 3 ) ( list 4 5 6 ) )

	- LAMBDA et Call-CC isn't implemented.

You can test this kind of functionnality.

REQUIREMENT :
-----------------
	-	JAVA
	- 	ANT Build
	-   GIT 


How to Build you're project :
-----------------------------------

1) Open your consol on the source folder of this project
2) type "ant" to build this application and create the jar file.
3) type "java -jar jar/JScheme.jar" to run this application


You can test now this application.
On this project their don't have any syntaxe analyzer
make sure the commande jScheme is correct before you test it
Also all string have to be separeted by an espace.

( *  1 3 )  >> result 3
( -  5 7 )  >> result -2
( /  9 3 )  >> result 3

( let ( ( x 2 ) ( y 4 ) ) ( + x y ) )  >> result 6

( define f 2 ) >> result f

f  >> result 2

( list 1 2 3 4 ) >> result ( 1 2 3 4 )

( map add ( list 1 2 3 4 ) ( list 5 6 7 8 ))  >> result ( 6 8 10 12 )
